import axios from "axios";



const Repository = axios.create({
  baseURL: process.env.VUE_APP_BASEURL
});

const token = window.localStorage.getItem("token");


if (token) Repository.defaults.headers.common.Authorization = "Bearer " + token;

export default Repository;
