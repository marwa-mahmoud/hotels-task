export default {
  namespaced: true,

  state: {
    fromDate: null,
    toDate: null,
  },

  mutations: {
    SET_FROM_DATE(state, fromDate) {
      state.fromDate = fromDate;
    },
    SET_TO_DATE(state, toDate) {
      state.toDate = toDate;
    },
  },
  actions: {}
};
