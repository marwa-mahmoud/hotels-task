// src/main.js
import "babel-polyfill";
import Vue from "vue";
import VueRouter from "vue-router";
import store from "./store";
import VueToast from "vue-toast-notification";
import * as VueGoogleMaps from "vue2-google-maps";
import "vue-toast-notification/dist/index.css";
import "./plugins/veeValidate";

// CASL
import { abilitiesPlugin } from "@casl/vue";
Vue.use(abilitiesPlugin);

// Vuex
import Vuex from "vuex";
Vue.use(Vuex);
Vue.use(VueToast);

// Vuetify
import vuetify from "@/plugins/vuetify";

// fontawsome
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUserSecret } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import moment from 'moment';

library.add(faUserSecret);

Vue.component("font-awesome-icon", FontAwesomeIcon);

// Date Format

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY')
  }
});
Vue.filter('formatTime', function(value) {
  if (value) {
    return moment(String(value)).format('H:mm:ss')
  }
});

// google maps

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCwMcXU4txrGHqWgvO0PQQ_YL7L2rgPRP8",
    v: "3.30",
    libraries: "places"
  }
});

// My App
import App from "./App.vue";
import router from "./router";

// My router
Vue.config.productionTip = false;
Vue.use(VueRouter);

new Vue({
  render: h => h(App),
  vuetify,
  store,
  router
}).$mount("#app");
