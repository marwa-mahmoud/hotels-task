import "@mdi/font/css/materialdesignicons.css";
import Vue from "vue";

// Tibtab Html Editor
import { TiptapVuetifyPlugin } from "tiptap-vuetify";
import "tiptap-vuetify/dist/main.css";

import { Ripple } from "vuetify/lib/directives";

import Vuetify, {
  VSlider,
  VStepperStep,
  VStepper,
  VStepperHeader,
  VSubheader,
  VSelect,
  VApp,
  VNavigationDrawer,
  VContent,
  VContainer,
  VList,
  VListItem,
  VTabItem,
  VFileInput,
  VListItemIcon,
  VIcon,
  VListItemTitle,
  VListItemGroup,
  VListItemActionText,
  VListItemAction,
  VListItemSubtitle,
  VListItemContent,
  VDivider,
  VAlert,
  VDataTable,
  VBtn,
  VTabs,
  VTabsSlider,
  VLayout,
  VFlex,
  VCard,
  VCardTitle,
  VCardText,
  VCardActions,
  VForm,
  VTextField,
  VTextarea,
  VSnackbar,
  VDialog,
  VTab,
  VTabsItems,
  VSpacer,
  VAutocomplete,
  VTooltip,
  VToolbar,
  VFooter,
  VToolbarTitle,
  VToolbarItems,
  VMenu,
  VRow,
  VCol,
  VTreeview,
  VProgressCircular,
  VSlideXReverseTransition,
  VRadio,
  VCheckbox,
  VRadioGroup,
  VDatePicker
} from "vuetify/lib";

Vue.use(Vuetify, {
  components: {
    VSlider,
    VStepperStep,
    VCheckbox,
    VStepper,
    VStepperHeader,
    VSubheader,
    VFileInput,
    VApp,
    VNavigationDrawer,
    VSelect,
    VContent,
    VTabItem,
    VContainer,
    VList,
    VTabs,
    VTabsSlider,
    VTab,
    VTabsItems,
    VListItem,
    VListItemIcon,
    VListItemAction,
    VListItemGroup,
    VListItemActionText,
    VIcon,
    VListItemTitle,
    VListItemSubtitle,
    VListItemContent,
    VDivider,
    VAlert,
    VDataTable,
    VBtn,
    VLayout,
    VFlex,
    VCard,
    VCardTitle,
    VCardText,
    VCardActions,
    VForm,
    VTextField,
    VTextarea,
    VSnackbar,
    VDialog,
    VSpacer,
    VAutocomplete,
    VTooltip,
    VToolbar,
    VFooter,
    VToolbarTitle,
    VToolbarItems,
    VMenu,
    VRow,
    VCol,
    VTreeview,
    VProgressCircular,
    VSlideXReverseTransition,
    VRadio,
    VRadioGroup,
    VDatePicker
  },
  directives: {
    Ripple
  }
});

Vue.use(TiptapVuetifyPlugin, {
  iconsGroup: "mdi"
});



export default new Vuetify({
  icons: {
    iconfont: "mdi"
  },
  theme: {
    themes: {
      light: {
        primary: "#F0BC5E",
        secondary: "#b0bec5",
        accent: "#8c9eff",
        error: "#b71c1c"
      }
    }
  },

});
