import homeSearch from '@/modules/search/homeSearch.vue';
import searchResults from '@/modules/search/searchResults.vue'


export default [
	{
		path: '/',
		name: 'homeSearch',
		component: homeSearch,
		meta: { showDrawer: false },
	},
	{
		path: '/search-results',
		name: 'searchResults',
		component: searchResults
	},

]